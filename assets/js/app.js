//JQuery Module Pattern

// An object literal
var app = {
  init: function() {
    app.functionOne();
  },
  functionOne: function () {
  }
};
$("document").ready(function () {
  app.init();
  $('#btn-header-search').click(function(){
    $('.header-search').fadeToggle();
  });

  $('#btn-compare-hide').click(function(){
    $('#compare').toggleClass('hide');
    $('#btn-compare-hide span p').text(function(i, text){
      return text === "GÖSTER" ? "GİZLE" : "GÖSTER";
  });
  $('#btn-compare-hide i').toggleClass('icon-rotate');
  });

  $('.navbar-nav .nav-item').click(function(){
    $('.navbar-nav .nav-item').removeClass('active');
    $(this).addClass('active');
  });

  $('#imageGallery').lightSlider({
    gallery:true,
    item:1,
    loop:true,
    thumbItem:9,
    slideMargin:0,
    enableDrag: false,
    currentPagerPosition:'left',
    onSliderLoad: function(el) {
        el.lightGallery({
            selector: '#imageGallery .lslide'
        });
    }
  });
});
$(window).on('load', function(){
  $('#splashScreen').fadeOut(1000);
})
